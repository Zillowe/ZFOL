<div align="center">
<h1>Zillowe Foundation Open License</h1>
Date: February 16, 2025 <br>
Version: 1.0.2
</div>


## Summary

1. **You can:**

   - Use, modify, reproduce, distribute, translate, adapt, and publicly display the Work;

   - Create derivative works from the Work and distribute them under a different license.


2. **Restrictions:**

   - You cannot use the copyright holder's brand without permission;

   - This license does not grant rights to patents, trade secrets, etc.


3. **No warranty:**

   - The Work is provided "as is", with no guarantees;

   - The copyright holder is not liable for any damages.


4. **Termination:** If you break the terms, the license terminates, and you must stop using the Work.


5. **Self-governing:** There's no external enforcement, you are responsible for following the terms.


6. **Entire Agreement:** This license is the complete agreement, overriding any prior agreements.


7. **Severability:** If a part of the license is found invalid, the rest stays enforceable.



## Badges

![ZFOL](https://codeberg.org/Zillowe/ZFOL/raw/branch/main/badges/1-0/dark.svg) | Dark

![ZFOL](https://codeberg.org/Zillowe/ZFOL/raw/branch/main/badges/1-0/light.svg) | Light


You can use them in repository to show that you're using this license, just add it to your files:


`README.md`

```markdown
<!-- dark -->

![ZFOL](https://codeberg.org/Zillowe/ZFOL/raw/branch/main/badges/1-0/dark.svg)

<!-- light -->

![ZFOL](https://codeberg.org/Zillowe/ZFOL/raw/branch/main/badges/1-0/light.svg)
```

`index.html`

```html
<!-- dark -->

<img
  alt="ZFOL"
  src="https://codeberg.org/Zillowe/ZFOL/raw/branch/main/badges/1-0/dark.svg"
/>

<!-- light -->

<img
  alt="ZFOL"
  src="https://codeberg.org/Zillowe/ZFOL/raw/branch/main/badges/1-0/light.svg"
/>
```