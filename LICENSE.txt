Zillowe Foundation Open License
Date: February 16, 2025
Version: 1.0.2

1. Definitions

1.1. "You" (or "Your") refers to any person who obtains a copy
     of the Work.

1.2. "Work" refers to the creative material, including but not
     limited to, source code, data, images, sounds, text, or
     any other content provided under this license.

1.3. "Brand" refers to any trademarks, service marks, trade names,
     logos, or other identifying marks owned by the copyright
     holder(s) of the Work.

1.4. "Derivative Work" means a work that is substantially derived
     from the Original Work, but which incorporates additional or
     modified material.

2. Grant of Rights

2.1. Subject to the restrictions below, you are granted the broadest
     possible worldwide, non-exclusive, irrevocable, royalty-free,
     perpetual license to use, modify, reproduce, distribute, sublicense,
     translate, adapt, publicly display, and perform the Work, and to
     create Derivative Works. You are also expressly granted the right
     to distribute any Derivative Work created from the Work under a
     different license.

3. Restrictions

3.1. You may not use the Brand without the prior written permission
     of the copyright holder(s).

3.2. This license does not grant you any rights to patents, trade
     secrets, or other intellectual property rights not expressly
     included in this license.

4. Termination

4.1. This license terminates if you fail to comply with any of its
     terms. Upon termination, you must cease all use and distribution
     of the Work and any Derivative Works.

5. Entire Agreement

5.1. This license constitutes the entire agreement between you and the
     copyright holder(s) of the Work and supersedes all prior or
     contemporaneous communications, representations, or agreements,
     whether oral or written.

6. Severability

6.1. If any provision of this license is held to be invalid or
     unenforceable, such provision shall be struck and the
     remaining provisions shall remain in full force and effect.

7. Disclaimer of External Enforcement

7.1. This license is self-governing. You acknowledge there may be no
     external enforcement mechanism for this license, and you agree to
     be solely responsible for your compliance with its terms.

8. Disclaimer of Warranty

8.1. The Work is provided "as is" and without warranty of any kind,
     express, implied, or statutory. The copyright holder(s) shall not
     be liable for any damages arising out of or in connection with
     your use of the Work.